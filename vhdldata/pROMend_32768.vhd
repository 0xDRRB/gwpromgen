    dff_inst_0: DFFE
        port map (
            Q => dff_q_0,
            D => ad(14),
            CLK => clk,
            CE => ce
        );

    mux_inst_0: MUX2
        port map (
            O => dout(0),
            I0 => prom_inst_0_dout(0),
            I1 => prom_inst_1_dout(0),
            S0 => dff_q_0
        );

    mux_inst_1: MUX2
        port map (
            O => dout(1),
            I0 => prom_inst_2_dout(1),
            I1 => prom_inst_3_dout(1),
            S0 => dff_q_0
        );

    mux_inst_2: MUX2
        port map (
            O => dout(2),
            I0 => prom_inst_4_dout(2),
            I1 => prom_inst_5_dout(2),
            S0 => dff_q_0
        );

    mux_inst_3: MUX2
        port map (
            O => dout(3),
            I0 => prom_inst_6_dout(3),
            I1 => prom_inst_7_dout(3),
            S0 => dff_q_0
        );

    mux_inst_4: MUX2
        port map (
            O => dout(4),
            I0 => prom_inst_8_dout(4),
            I1 => prom_inst_9_dout(4),
            S0 => dff_q_0
        );

    mux_inst_5: MUX2
        port map (
            O => dout(5),
            I0 => prom_inst_10_dout(5),
            I1 => prom_inst_11_dout(5),
            S0 => dff_q_0
        );

    mux_inst_6: MUX2
        port map (
            O => dout(6),
            I0 => prom_inst_12_dout(6),
            I1 => prom_inst_13_dout(6),
            S0 => dff_q_0
        );

    mux_inst_7: MUX2
        port map (
            O => dout(7),
            I0 => prom_inst_14_dout(7),
            I1 => prom_inst_15_dout(7),
            S0 => dff_q_0
        );

end Behavioral; --single_port_rom
