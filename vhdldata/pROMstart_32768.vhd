library IEEE;
use IEEE.std_logic_1164.all;

entity single_port_rom is
    port (
        dout: out std_logic_vector(7 downto 0);
        clk: in std_logic;
        oce: in std_logic;
        ce: in std_logic;
        reset: in std_logic;
        ad: in std_logic_vector(14 downto 0)
    );
end single_port_rom;

architecture Behavioral of single_port_rom is

    signal lut_f_0: std_logic;
    signal lut_f_1: std_logic;
    signal prom_inst_0_dout_w: std_logic_vector(30 downto 0);
    signal prom_inst_0_dout: std_logic_vector(0 downto 0);
    signal prom_inst_1_dout_w: std_logic_vector(30 downto 0);
    signal prom_inst_1_dout: std_logic_vector(0 downto 0);
    signal prom_inst_2_dout_w: std_logic_vector(30 downto 0);
    signal prom_inst_2_dout: std_logic_vector(1 downto 1);
    signal prom_inst_3_dout_w: std_logic_vector(30 downto 0);
    signal prom_inst_3_dout: std_logic_vector(1 downto 1);
    signal prom_inst_4_dout_w: std_logic_vector(30 downto 0);
    signal prom_inst_4_dout: std_logic_vector(2 downto 2);
    signal prom_inst_5_dout_w: std_logic_vector(30 downto 0);
    signal prom_inst_5_dout: std_logic_vector(2 downto 2);
    signal prom_inst_6_dout_w: std_logic_vector(30 downto 0);
    signal prom_inst_6_dout: std_logic_vector(3 downto 3);
    signal prom_inst_7_dout_w: std_logic_vector(30 downto 0);
    signal prom_inst_7_dout: std_logic_vector(3 downto 3);
    signal prom_inst_8_dout_w: std_logic_vector(30 downto 0);
    signal prom_inst_8_dout: std_logic_vector(4 downto 4);
    signal prom_inst_9_dout_w: std_logic_vector(30 downto 0);
    signal prom_inst_9_dout: std_logic_vector(4 downto 4);
    signal prom_inst_10_dout_w: std_logic_vector(30 downto 0);
    signal prom_inst_10_dout: std_logic_vector(5 downto 5);
    signal prom_inst_11_dout_w: std_logic_vector(30 downto 0);
    signal prom_inst_11_dout: std_logic_vector(5 downto 5);
    signal prom_inst_12_dout_w: std_logic_vector(30 downto 0);
    signal prom_inst_12_dout: std_logic_vector(6 downto 6);
    signal prom_inst_13_dout_w: std_logic_vector(30 downto 0);
    signal prom_inst_13_dout: std_logic_vector(6 downto 6);
    signal prom_inst_14_dout_w: std_logic_vector(30 downto 0);
    signal prom_inst_14_dout: std_logic_vector(7 downto 7);
    signal prom_inst_15_dout_w: std_logic_vector(30 downto 0);
    signal prom_inst_15_dout: std_logic_vector(7 downto 7);
    signal dff_q_0: std_logic;
    signal prom_inst_0_DO_o: std_logic_vector(31 downto 0);
    signal prom_inst_1_DO_o: std_logic_vector(31 downto 0);
    signal prom_inst_2_DO_o: std_logic_vector(31 downto 0);
    signal prom_inst_3_DO_o: std_logic_vector(31 downto 0);
    signal prom_inst_4_DO_o: std_logic_vector(31 downto 0);
    signal prom_inst_5_DO_o: std_logic_vector(31 downto 0);
    signal prom_inst_6_DO_o: std_logic_vector(31 downto 0);
    signal prom_inst_7_DO_o: std_logic_vector(31 downto 0);
    signal prom_inst_8_DO_o: std_logic_vector(31 downto 0);
    signal prom_inst_9_DO_o: std_logic_vector(31 downto 0);
    signal prom_inst_10_DO_o: std_logic_vector(31 downto 0);
    signal prom_inst_11_DO_o: std_logic_vector(31 downto 0);
    signal prom_inst_12_DO_o: std_logic_vector(31 downto 0);
    signal prom_inst_13_DO_o: std_logic_vector(31 downto 0);
    signal prom_inst_14_DO_o: std_logic_vector(31 downto 0);
    signal prom_inst_15_DO_o: std_logic_vector(31 downto 0);

    -- component declaration
    component LUT2
        generic (
            INIT: in bit_vector := X"0"
        );
        port (
            F: out std_logic;
            I0: in std_logic;
            I1: in std_logic
        );
    end component;

    --component declaration
    component pROM
        generic (
            READ_MODE: in bit :='0';
            BIT_WIDTH: in integer := 9;
            RESET_MODE: in string := "SYNC";
            INIT_RAM_00: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_01: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_02: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_03: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_04: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_05: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_06: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_07: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_08: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_09: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_0A: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_0B: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_0C: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_0D: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_0E: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_0F: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_10: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_11: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_12: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_13: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_14: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_15: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_16: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_17: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_18: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_19: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_1A: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_1B: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_1C: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_1D: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_1E: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_1F: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_20: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_21: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_22: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_23: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_24: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_25: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_26: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_27: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_28: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_29: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_2A: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_2B: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_2C: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_2D: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_2E: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_2F: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_30: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_31: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_32: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_33: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_34: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_35: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_36: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_37: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_38: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_39: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_3A: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_3B: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_3C: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_3D: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_3E: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000";
            INIT_RAM_3F: in bit_vector := X"0000000000000000000000000000000000000000000000000000000000000000"
        );
        port (
            DO: out std_logic_vector(31 downto 0);
            CLK: in std_logic;
            OCE: in std_logic;
            CE: in std_logic;
            RESET: in std_logic;
            AD: in std_logic_vector(13 downto 0)
        );
    end component;

    -- component declaration
    component DFFE
        port (
            Q: out std_logic;
            D: in std_logic;
            CLK: in std_logic;
            CE: in std_logic
        );
    end component;

    -- component declaration
    component MUX2
        port (
            O: out std_logic;
            I0: in std_logic;
            I1: in std_logic;
            S0: in std_logic
        );
    end component;

begin
    prom_inst_0_dout(0) <= prom_inst_0_DO_o(0);
    prom_inst_0_dout_w(30 downto 0) <= prom_inst_0_DO_o(31 downto 1) ;
    prom_inst_1_dout(0) <= prom_inst_1_DO_o(0);
    prom_inst_1_dout_w(30 downto 0) <= prom_inst_1_DO_o(31 downto 1) ;
    prom_inst_2_dout(1) <= prom_inst_2_DO_o(0);
    prom_inst_2_dout_w(30 downto 0) <= prom_inst_2_DO_o(31 downto 1) ;
    prom_inst_3_dout(1) <= prom_inst_3_DO_o(0);
    prom_inst_3_dout_w(30 downto 0) <= prom_inst_3_DO_o(31 downto 1) ;
    prom_inst_4_dout(2) <= prom_inst_4_DO_o(0);
    prom_inst_4_dout_w(30 downto 0) <= prom_inst_4_DO_o(31 downto 1) ;
    prom_inst_5_dout(2) <= prom_inst_5_DO_o(0);
    prom_inst_5_dout_w(30 downto 0) <= prom_inst_5_DO_o(31 downto 1) ;
    prom_inst_6_dout(3) <= prom_inst_6_DO_o(0);
    prom_inst_6_dout_w(30 downto 0) <= prom_inst_6_DO_o(31 downto 1) ;
    prom_inst_7_dout(3) <= prom_inst_7_DO_o(0);
    prom_inst_7_dout_w(30 downto 0) <= prom_inst_7_DO_o(31 downto 1) ;
    prom_inst_8_dout(4) <= prom_inst_8_DO_o(0);
    prom_inst_8_dout_w(30 downto 0) <= prom_inst_8_DO_o(31 downto 1) ;
    prom_inst_9_dout(4) <= prom_inst_9_DO_o(0);
    prom_inst_9_dout_w(30 downto 0) <= prom_inst_9_DO_o(31 downto 1) ;
    prom_inst_10_dout(5) <= prom_inst_10_DO_o(0);
    prom_inst_10_dout_w(30 downto 0) <= prom_inst_10_DO_o(31 downto 1) ;
    prom_inst_11_dout(5) <= prom_inst_11_DO_o(0);
    prom_inst_11_dout_w(30 downto 0) <= prom_inst_11_DO_o(31 downto 1) ;
    prom_inst_12_dout(6) <= prom_inst_12_DO_o(0);
    prom_inst_12_dout_w(30 downto 0) <= prom_inst_12_DO_o(31 downto 1) ;
    prom_inst_13_dout(6) <= prom_inst_13_DO_o(0);
    prom_inst_13_dout_w(30 downto 0) <= prom_inst_13_DO_o(31 downto 1) ;
    prom_inst_14_dout(7) <= prom_inst_14_DO_o(0);
    prom_inst_14_dout_w(30 downto 0) <= prom_inst_14_DO_o(31 downto 1) ;
    prom_inst_15_dout(7) <= prom_inst_15_DO_o(0);
    prom_inst_15_dout_w(30 downto 0) <= prom_inst_15_DO_o(31 downto 1) ;
    lut_inst_0 : LUT2
        generic map (
            INIT => X"2"
        )
        port map (
            F => lut_f_0,
            I0 => ce,
            I1 => ad(14)
        );

    lut_inst_1 : LUT2
        generic map (
            INIT => X"8"
        )
        port map (
            F => lut_f_1,
            I0 => ce,
            I1 => ad(14)
        );

