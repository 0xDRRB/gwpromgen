# VHDL ROM generator for Gowin IP Core

The Gowin IDE includes a code generator for using the IP Cores supplied with the environment. Unfortunately, as far as pROM is concerned, and in particular the initialization of stored data, it is only during code generation that it is possible to specify a MIF file. Altera/Intel's Quartus II, for example, allows you to use Verilog or VHDL code referencing the MIF file, without having to re-generate the VHDL code.

This poses a problem with the Gowin environment, since it is not possible to change pROM data from the command line with a Tcl script (`gw_sh`). Generating pROM from binary data cannot be done with a `Makefile`.

For one of my projects (CPU Z80 + RAM + ROM + UART with a Tang Nano 9k), I created `gwpromgen`, a VHDL code generator producing a pROM entity from a binary file compiled with SDCC. `gwpromgen` is not a fully customizable generator like the one integrated in the IDE. It simply stitches together pieces of VHDL code (see `vhdldata/`) and adds re-encoded data from the binary file. This file must be padded to the size of the pROM, and this size determines the code generated (1024, 2048, 4096, 8192, 16384, or 32768 bytes).

`gwpromgen` was originally designed to be used in a `Makefile` to automate the generation of a pROM entity containing data from a compilation for a softcore (Z80, 68k, 6502, etc.). For the time being, I don't intend to extend this to the Verilog language or to support other pROM sizes.

This code has been tested with a Tang Nano 9K board, but it's highly likely that it will also work with the manufacturer's other boards/FPGAs, such as the Tang Nano 20K.
