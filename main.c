/*
 * Copyright (c) 2024 Denis Bodor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIEDi
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <err.h>
#include <getopt.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#define MAXSIZE 32768

#define FTYPE_START 0
#define FTYPE_END   1

#define SEARCH_PATH "vhdldata"

#define STARTFILE_1024 "pROMstart_1024.vhd"
#define ENDFILE_1024  "pROMend_1024.vhd"

#define STARTFILE_2048 "pROMstart_2048.vhd"
#define ENDFILE_2048  "pROMend_2048.vhd"

#define STARTFILE_4096 "pROMstart_4096.vhd"
#define ENDFILE_4096  "pROMend_4096.vhd"

#define STARTFILE_8192 "pROMstart_8192.vhd"
#define ENDFILE_8192  "pROMend_8192.vhd"

#define STARTFILE_16384 "pROMstart_16384.vhd"
#define ENDFILE_16384  "pROMend_16384.vhd"

#define STARTFILE_32768 "pROMstart_32768.vhd"
#define ENDFILE_32768  "pROMend_32768.vhd"

void printvhdl1k(unsigned char *convdata, FILE *out)
{
	int i, j;

	if (!out)
		out = stdout;

	fprintf(out, "    prom_inst_0: pROM\n");
	fprintf(out, "        generic map (\n");
	fprintf(out, "            READ_MODE => '0',\n");
	fprintf(out, "            BIT_WIDTH => 8,\n");
	fprintf(out, "            RESET_MODE => \"SYNC\",\n");

	// print in reverse order
	for (i = 0; i < 32*32; i += 32) {
		fprintf(out, "            INIT_RAM_%02X => X\"", i/32);
		for (j = 0; j < 32; j++) {
			fprintf(out, "%02X", convdata[i+31-j]);
		}
		fprintf(out, "\"");
		if (i/32 != 0x1f) // last line, no ','
			fprintf(out, ",");
		fprintf(out, "\n");
	}

	fprintf(out, "        )\n");
	fprintf(out, "        port map (\n");
	fprintf(out, "            DO => prom_inst_0_DO_o,\n");
	fprintf(out, "            CLK => clk,\n");
	fprintf(out, "            OCE => oce,\n");
	fprintf(out, "            CE => ce,\n");
	fprintf(out, "            RESET => reset,\n");
	fprintf(out, "            AD => prom_inst_0_AD_i\n");
	fprintf(out, "        );\n");
	fprintf(out, "\n");
}

void printvhdl2k(unsigned char *convdata, FILE *out)
{
	int i, j;

	if (!out)
		out = stdout;

	fprintf(out, "    prom_inst_0: pROM\n");
	fprintf(out, "        generic map (\n");
	fprintf(out, "            READ_MODE => '0',\n");
	fprintf(out, "            BIT_WIDTH => 8,\n");
	fprintf(out, "            RESET_MODE => \"SYNC\",\n");

	// print in reverse order
	for (i = 0; i < 32*64; i += 32) {
		fprintf(out, "            INIT_RAM_%02X => X\"", i/32);
		for (j = 0; j < 32; j++) {
			fprintf(out, "%02X", convdata[i+31-j]);
		}
		fprintf(out, "\"");
		if (i/32 != 0x3f) // last line, no ','
			fprintf(out, ",");
		fprintf(out, "\n");
	}

	fprintf(out, "        )\n");
	fprintf(out, "        port map (\n");
	fprintf(out, "            DO => prom_inst_0_DO_o,\n");
	fprintf(out, "            CLK => clk,\n");
	fprintf(out, "            OCE => oce,\n");
	fprintf(out, "            CE => ce,\n");
	fprintf(out, "            RESET => reset,\n");
	fprintf(out, "            AD => prom_inst_0_AD_i\n");
	fprintf(out, "        );\n");
	fprintf(out, "\n");
}

void convert4k(unsigned char *source, unsigned char *target)
{
	int i;

	/*
	 * inst0 bits 0-3 of each byte : 2048 kio, 16384 bits (16kb)
	 * inst1 bits 4-8 of each byte : 2048 kio, 16384 bits (16kb)
	 */
	for (i = 0; i < 4096; i += 2) {
		target[i / 2] = (source[i] & 0x0f) | ((source[i + 1] & 0x0f) << 4);
		target[2048 + i / 2] = ((source[i] >> 4) & 0x0f) | (((source[i + 1] >> 4) & 0x0f) << 4);
	}
}

void printvhdl4k(unsigned char *convdata, FILE *out)
{
	int i, j;
	int instance;

	if (!out)
		out = stdout;

	for (instance = 0; instance < 2; instance++) {
		fprintf(out, "    prom_inst_%u: pROM\n", instance);
		fprintf(out, "        generic map (\n");
		fprintf(out, "            READ_MODE => '0',\n");
		fprintf(out, "            BIT_WIDTH => 4,\n");
		fprintf(out, "            RESET_MODE => \"SYNC\",\n");

		// print in reverse order
		for (i = 0; i < 32*64; i += 32) {
			fprintf(out, "            INIT_RAM_%02X => X\"", i/32);
			for (j = 31; j >= 0; j--) {
				fprintf(out, "%02X", convdata[(2048 * instance) + i + j]);
			}
			fprintf(out, "\"");
			if (i/32 != 0x3f) // last line, no ','
				fprintf(out, ",");
			fprintf(out, "\n");
		}

		fprintf(out, "        )\n");
		fprintf(out, "        port map (\n");
		fprintf(out, "            DO => prom_inst_%u_DO_o,\n", instance);
		fprintf(out, "            CLK => clk,\n");
		fprintf(out, "            OCE => oce,\n");
		fprintf(out, "            CE => ce,\n");
		fprintf(out, "            RESET => reset,\n");
		fprintf(out, "            AD => prom_inst_%u_AD_i\n", instance);
		fprintf(out, "        );\n");
		fprintf(out, "\n");
	}
}

void convert8k(unsigned char *source, unsigned char *target)
{
	int i;

	/*
	 * inst0 bits 0-1 of each byte : 2048 kio, 16384 bits (16kb)
	 * inst1 bits 2-3 of each byte : 2048 kio, 16384 bits (16kb)
	 * inst2 bits 4-5 of each byte : 2048 kio, 16384 bits (16kb)
	 * inst4 bits 6-6 of each byte : 2048 kio, 16384 bits (16kb)
	 */
	for (i = 0; i < 8192; i += 4) {
		target[i / 4] = (source[i] & 0x03) | ((source[i + 1] & 0x03) << 2) | ((source[i + 2] & 0x03) << 4) | ((source[i + 3] & 0x03) << 6);
		target[2048 + i / 4] = ((source[i] >> 2) & 0x03) | (((source[i + 1] >> 2) & 0x03) << 2) | (((source[i + 2] >> 2) & 0x03) << 4) | (((source[i + 3] >> 2) & 0x03) << 6);
		target[4096 + i / 4] = ((source[i] >> 4) & 0x03) | (((source[i + 1] >> 4) & 0x03) << 2) | (((source[i + 2] >> 4) & 0x03) << 4) | (((source[i + 3] >> 4) & 0x03) << 6);
		target[6144 + i / 4] = ((source[i] >> 6) & 0x03) | (((source[i + 1] >> 6) & 0x03) << 2) | (((source[i + 2] >> 6) & 0x03) << 4) | (((source[i + 3] >> 6) & 0x03) << 6);
	}
}

void printvhdl8k(unsigned char *convdata, FILE *out)
{
	int i, j;
	int instance;

	if (!out)
		out = stdout;

	for (instance = 0; instance < 4; instance++) {
		fprintf(out, "    prom_inst_%u: pROM\n", instance);
		fprintf(out, "        generic map (\n");
		fprintf(out, "            READ_MODE => '0',\n");
		fprintf(out, "            BIT_WIDTH => 2,\n");
		fprintf(out, "            RESET_MODE => \"SYNC\",\n");

		// print in reverse order
		for (i = 0; i < 32*64; i += 32) {
			fprintf(out, "            INIT_RAM_%02X => X\"", i/32);
			for (j = 31; j >= 0; j--) {
				fprintf(out, "%02X", convdata[(2048 * instance) + i + j]);
			}
			fprintf(out, "\"");
			if (i/32 != 0x3f) // last line, no ','
				fprintf(out, ",");
			fprintf(out, "\n");
		}

		fprintf(out, "        )\n");
		fprintf(out, "        port map (\n");
		fprintf(out, "            DO => prom_inst_%u_DO_o,\n", instance);
		fprintf(out, "            CLK => clk,\n");
		fprintf(out, "            OCE => oce,\n");
		fprintf(out, "            CE => ce,\n");
		fprintf(out, "            RESET => reset,\n");
		fprintf(out, "            AD => prom_inst_%u_AD_i\n", instance);
		fprintf(out, "        );\n");
		fprintf(out, "\n");
	}
}

void convert16k(unsigned char *source, unsigned char *target)
{
	int b, i, j;

	/*
	 * What we want :
	 *   inst0 bits 0 of each byte : 2048 kio, 16384 bits (16kb)
	 *   inst1 bits 1 of each byte : 2048 kio, 16384 bits (16kb)
	 *   inst2 bits 2 of each byte : 2048 kio, 16384 bits (16kb)
	 *   inst3 bits 3 of each byte : 2048 kio, 16384 bits (16kb)
	 *   inst4 bits 4 of each byte : 2048 kio, 16384 bits (16kb)
	 *   inst5 bits 5 of each byte : 2048 kio, 16384 bits (16kb)
	 *   inst6 bits 6 of each byte : 2048 kio, 16384 bits (16kb)
	 *   inst7 bits 7 of each byte : 2048 kio, 16384 bits (16kb)
	*/
	for (b = 0; b < 8; b++) {
		for (i = 0; i < 16384; i += 8) {
			for (j = 0; j < 8; j++) { // 8 bits at pos b
				target[(2048 * b) + i / 8] |= ((source[i + j] >> b) & 0x01) << j;
			}
		}
	}
}

void printvhdl16k(unsigned char *convdata, FILE *out)
{
	int i, j;
	int instance;
	int partCE = 0;

	if (!out)
		out = stdout;

	for (instance = 0; instance < 8; instance++) {
		fprintf(out, "    prom_inst_%d: pROM\n", instance);
		fprintf(out, "        generic map (\n");
		fprintf(out, "            READ_MODE => '0',\n");
		fprintf(out, "            BIT_WIDTH => 1,\n");
		fprintf(out, "            RESET_MODE => \"SYNC\",\n");

		// print in reverse order
		for (i = 0; i < 32*64; i += 32) {
			fprintf(out, "            INIT_RAM_%02X => X\"", i/32);
			for (j = 31; j >= 0; j--) {
				fprintf(out, "%02X", convdata[(2048 * instance) + i + j]);
			}
			fprintf(out, "\"");
			if (i/32 != 0x3f) // last line, no ','
				fprintf(out, ",");
			fprintf(out, "\n");
		}

		fprintf(out, "        )\n");
		fprintf(out, "        port map (\n");
		fprintf(out, "            DO => prom_inst_%u_DO_o,\n", instance);
		fprintf(out, "            CLK => clk,\n");
		fprintf(out, "            OCE => oce,\n");
		fprintf(out, "            CE => ce,\n");
		fprintf(out, "            RESET => reset,\n");
		fprintf(out, "            AD => ad(13 downto 0)\n");
		fprintf(out, "        );\n");
		fprintf(out, "\n");

		partCE = !partCE;
	}
}

void convert32k(unsigned char *source, unsigned char *target)
{
	int b, i, j;

	/*
	 * What we want :
	 *   inst0  bits 0 of each byte 1/2 : 2048 kio, 16384 bits (16kb)
	 *   inst1  bits 0 of each byte 2/2 : 2048 kio, 16384 bits (16kb)
	 *   inst2  bits 1 of each byte 1/2 : 2048 kio, 16384 bits (16kb)
	 *   inst3  bits 1 of each byte 2/2 : 2048 kio, 16384 bits (16kb)
	 *   inst4  bits 2 of each byte 1/2 : 2048 kio, 16384 bits (16kb)
	 *   inst5  bits 2 of each byte 2/2 : 2048 kio, 16384 bits (16kb)
	 *   inst6  bits 3 of each byte 1/2 : 2048 kio, 16384 bits (16kb)
	 *   inst7  bits 3 of each byte 2/2 : 2048 kio, 16384 bits (16kb)
	 *   inst8  bits 4 of each byte 1/2 : 2048 kio, 16384 bits (16kb)
	 *   inst9  bits 4 of each byte 2/2 : 2048 kio, 16384 bits (16kb)
	 *   inst10 bits 5 of each byte 1/2 : 2048 kio, 16384 bits (16kb)
	 *   inst11 bits 5 of each byte 2/2 : 2048 kio, 16384 bits (16kb)
	 *   inst12 bits 6 of each byte 1/2 : 2048 kio, 16384 bits (16kb)
	 *   inst13 bits 6 of each byte 2/2 : 2048 kio, 16384 bits (16kb)
	 *   inst14 bits 7 of each byte 1/2 : 2048 kio, 16384 bits (16kb)
	 *   inst15 bits 7 of each byte 2/2 : 2048 kio, 16384 bits (16kb)
	*/
	for (b = 0; b < 8; b++) {
		for (i = 0; i < 32768; i += 8) {
			for (j = 0; j < 8; j++) { // 8 bits at pos b
				target[(4096 * b) + i / 8] |= ((source[i + j] >> b) & 0x01) << j;
			}
		}
	}
}

void printvhdl32k(unsigned char *convdata, FILE *out)
{
	int i, j;
	int instance;
	int partCE = 0;

	if (!out)
		out = stdout;

	for (instance = 0; instance < 16; instance++) {
		fprintf(out, "    prom_inst_%d: pROM\n", instance);
		fprintf(out, "        generic map (\n");
		fprintf(out, "            READ_MODE => '0',\n");
		fprintf(out, "            BIT_WIDTH => 1,\n");
		fprintf(out, "            RESET_MODE => \"SYNC\",\n");

		// print in reverse order
		for (i = 0; i < 32*64; i += 32) {
			fprintf(out, "            INIT_RAM_%02X => X\"", i/32);
			for (j = 31; j >= 0; j--) {
				fprintf(out, "%02X", convdata[(2048 * instance) + i + j]);
			}
			fprintf(out, "\"");
			if (i/32 != 0x3f) // last line, no ','
				fprintf(out, ",");
			fprintf(out, "\n");
		}

		fprintf(out, "        )\n");
		fprintf(out, "        port map (\n");
		fprintf(out, "            DO => prom_inst_%u_DO_o,\n", instance);
		fprintf(out, "            CLK => clk,\n");
		fprintf(out, "            OCE => oce,\n");
		fprintf(out, "            CE => lut_f_%u,\n", partCE);
		fprintf(out, "            RESET => reset,\n");
		fprintf(out, "            AD => ad(13 downto 0)\n");
		fprintf(out, "        );\n");
		fprintf(out, "\n");

		partCE = !partCE;
	}
}

void printdata(unsigned char *data, size_t len, FILE *out)
{
	int i;

	if (!out)
		out = stdout;

	for (i = 0; i < len; i++)
		fprintf(out, "%c", data[i]);
}

int selectfile(char *filename, char *path, int type, size_t size)
{
	if (size != 1024 && size != 2048 && size != 4096 && size != 8192 && size != 16384 && size != 32768) {
		warnx("Invalid input file size! Must be 1024, 2048, 4096, 8192, 16384 or 32768 bytes.");
		return(-1);
	}

	if (type == FTYPE_START) {
		if (snprintf(filename, 4096, "%s/pROMstart_%zu.vhd", path, size) >= 4096)
			return(-1);
	} else {
		if (snprintf(filename, 4096, "%s/pROMend_%zu.vhd", path, size) >= 4096)
			return(-1);
	}

	return(0);
}

void printhelp(char *binname)
{
	printf("VHDL code generator for GOWIN pROM IP Core v0.0.1\n");
	printf("Copyright (c) 2024 - Denis Bodor\n\n");
	printf("Usage : %s [OPTIONS]\n", binname);
	printf(" -i FILE     binary image to put in ROM\n");
	printf(" -s FILE     VHDL to put before generated code (default: depend on input file size)\n");
	printf(" -e FILE     VHDL to put after generated code (default: depend on input file size)\n");
	printf(" -p PATH     search path for default before/after VHDL files (default: ./vhdldata)\n");
	printf(" -o FILE     output to file (default is dump VHDL to STDOUT)\n");
}

int main (int argc, char**argv)
{
	int retopt;
	char *infilename = NULL;
	char startfilename[4096] = { 0 };
	char endfilename[4096] = { 0 };
	char *searchpath = NULL;
	char *outfilename = NULL;
	FILE *outfp = NULL;
	struct stat file_status;
	size_t binsize;
	size_t startsize;
	size_t endsize;
	int fd;
	unsigned char convdata[MAXSIZE] = { 0 };
	unsigned char *startdata;
	unsigned char *enddata;
	unsigned char *bindata;

	while ((retopt = getopt(argc, argv, "hi:s:e:p:o:")) != -1) {
		switch (retopt) {
		case 'i':
			infilename = strdup(optarg);
			break;
		case 's':
			if (snprintf(startfilename, 4096, "%s", optarg) >= 4096)
				errx(EXIT_FAILURE, "Error getting start filename!");
			break;
		case 'e':
			if (snprintf(endfilename, 4096, "%s", optarg) >= 4096)
				errx(EXIT_FAILURE, "Error getting end filename!");
			break;
		case 'p':
			searchpath = strdup(optarg);
			break;
		case 'o':
			outfilename = strdup(optarg);
			break;
		case 'h':
			printhelp(argv[0]);
			return(EXIT_SUCCESS);
		default:
			return(EXIT_FAILURE);
		}
	}

	if (!infilename) {
		errx(EXIT_FAILURE, "Need input file ! Use -h to view help.\n");
		return(EXIT_FAILURE);
	}

	if (stat(infilename, &file_status) == -1 ) {
		err(EXIT_FAILURE, "Error getting stats from binary file (%s)!", infilename);
	}

	binsize = file_status.st_size;

	if (!searchpath)
		searchpath = strdup(SEARCH_PATH);

	if (!strlen(startfilename))
		if (selectfile(startfilename, searchpath, FTYPE_START, binsize) != 0)
			errx(EXIT_FAILURE, "selectfile() error!");

	if (!strlen(endfilename))
		if (selectfile(endfilename, searchpath, FTYPE_END, binsize) != 0)
			errx(EXIT_FAILURE, "selectfile() error!");

	// mmap start file
	if (stat(startfilename, &file_status) == -1 ) {
		err(EXIT_FAILURE, "Error getting stats from start file (%s)!", startfilename);
	}
	startsize = file_status.st_size;
	if ((fd = open(startfilename, O_RDONLY)) == -1)
		err(EXIT_FAILURE, "Error opening start file!");
	if ((startdata = mmap(0, startsize, PROT_READ, MAP_SHARED, fd, 0)) == MAP_FAILED) {
		close(fd);
		err(EXIT_FAILURE, "Error mmapping start file!");
	}
	close(fd);

	// mmap bin data
	if ((fd = open(infilename, O_RDONLY)) == -1)
		err(EXIT_FAILURE, "Error opening binary file!");
	if ((bindata = mmap(0, binsize, PROT_READ, MAP_SHARED, fd, 0)) == MAP_FAILED) {
		close(fd);
		err(EXIT_FAILURE, "Error mmapping binary file!");
	}
	close(fd);

	// mmap end file
	if (stat(endfilename, &file_status) == -1 ) {
		err(EXIT_FAILURE, "Error getting stats from end file (%s)!", endfilename);
	}
	endsize = file_status.st_size;
	if ((fd = open(endfilename, O_RDONLY)) == -1)
		err(EXIT_FAILURE, "Error opening end file!");
	if ((enddata = mmap(0, endsize, PROT_READ, MAP_SHARED, fd, 0)) == MAP_FAILED) {
		close(fd);
		err(EXIT_FAILURE, "Error mmapping end file!");
	}
	close(fd);

	if (outfilename)
		if ((outfp = fopen(outfilename, "w" )) == NULL)
			err(EXIT_FAILURE, "Enable to open output file to write!");

	printdata(startdata, startsize, outfp);

	switch (binsize) {
	case 1024:
		printvhdl1k(bindata, outfp); // no data transform needed
		break;
	case 2048:
		printvhdl2k(bindata, outfp); // no data transform needed
		break;
	case 4096:
		convert4k(bindata, convdata);
		printvhdl4k(convdata, outfp);
		break;
	case 8192:
		convert8k(bindata, convdata);
		printvhdl8k(convdata, outfp);
		break;
	case 16384:
		convert16k(bindata, convdata);
		printvhdl16k(convdata, outfp);
		break;
	case 32768:
		convert32k(bindata, convdata);
		printvhdl32k(convdata, outfp);
		break;
	default:
	}

	printdata(enddata, endsize, outfp);

	// clean up
	if (outfp)
		fclose(outfp);

	if (munmap(startdata, startsize) == -1)
		err(EXIT_FAILURE, "Error un-mmapping start file");

	if (munmap(bindata, binsize) == -1)
		err(EXIT_FAILURE, "Error un-mmapping binary file");

	if (munmap(enddata, endsize) == -1)
		err(EXIT_FAILURE, "Error un-mmapping end file");

	if (searchpath)
		free(searchpath);

	if (infilename)
		free(infilename);

	if (outfilename)
		free(outfilename);

	return(EXIT_SUCCESS);
}
